# README #

## What is this repository for? ##

* Celero Code Samples

# AppScaffold #

Scaffolding of a sample application deployed with Docker.

### App consists of 3 components: ###
* Sql Server Database - consumed by webservices
* REST Web Services - ASP.NET Core WebAPI consumed by web
* Web Front End - Simple HTML5/JavaScript hosted in NGINX web server

### To build and deploy: ###
* docker-compose build
* docker-compose up

### Services will be running on following ports: ###
* Web Front End : http://localhost:1235
* Web Services: http://localhost:1234/api/values
* SqlServer: Server - localhost,1401  User - SA  Password: AbCd1234!

### Testing: ###
* CORS has not been configured in the docker containers so in order to test the web client you will need to run your browser with CORS disabled. In Chrome this can be done with the --disable-web-security --user-data-dir command line options
