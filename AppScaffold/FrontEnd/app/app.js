function ViewModel() {
    var self = this;

    // Members
    self.values = ko.observableArray();

    // Operations
    self.getValues = function() {
        console.log("In getValues()");
        rootUrl = 'http://localhost:1234/api/Values';
        $.ajax({
            url: rootUrl,
            type: 'GET',
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                self.values.removeAll();
                response.forEach(function(element) {
                    self.values.push(element);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error:" + jqXHR.responseText);
            }
        });    
    };
};

$(document).ready(function() {
});

ko.applyBindings(new ViewModel());