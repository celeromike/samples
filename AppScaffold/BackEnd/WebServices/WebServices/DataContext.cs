﻿using Microsoft.EntityFrameworkCore;
using WebServices.Models;

namespace WebServices
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public DbSet<Values> Values { get; set; }
    }
}
